#!/usr/bin/env python 
# -*- coding:utf-8 -*-
# !author:bruce chou
from wtforms import Form, StringField


class UserForm(Form):
    username = StringField(default=None)
    source = StringField(default=None)
