bigcode_MYSQL_DB = {
    'host': '106.15.63.203',
    'db': 'bigcode',
    'user': 'root',
    'port': 3306,
    'passwd': '75560zcb',
    'charset': 'utf8mb4'
}

LOCALHOST = '127.0.0.1'
REPO_PATH = '/home/xiaoquanbin/repository/repo'
REPO_ROOT_PATH_PATTERN_GITHUB = 'github.com/([\S]{1,})'

GITHUB_TOKEN = '1d09ada914c9f50240f618858b21d713ab0e9b1e'
GITHUB_API_URL_PREFIX = 'http://api.github.com/repos'
GITHUB_REMOTE_PREFIX = 'github.com'
CLONE_PROTOCOL_GITHUB = 'https'
GITHUB_API_USER_PREFIX = 'https://api.github.com/users'

KAFKA_LOG_PATH = '/home/xiaoquanbin/pythonApp/restfulAPI/log'

KAFKA_HOST = {
    'host-1': '127.0.0.1:9092'
}

REDIS = {
    'host': '127.0.0.1',
    'password': 'redis',
    'db': 5
}

KAFKA_TOPIC = {
    'topic-1': 'RepoManager',
    'topic-2': 'ProjectManager',
    'topic-3': 'CompleteDownload',
    'topic-4': 'repo_downloaded_r1p1',
    'topic-5': 'repo_updated_r1p1'
}

FLASK_APP_CONFIG = {
    'debug': False,
    'host': '0.0.0.0',
    'port': 8102,
    'threaded': True
}

DUPLICATE_UPPER_LIMIT = 20

RESTFULAPI = {
    'port-1': 8102,
    'port-2': 8001
}

GROUP = {
    'group-1': 'xqb:xqb'
}

CLONE_REPO = ['clone/hyperledger/fabric-master', 'clone/bitcoin/bitcoin-master', 'clone/ethereum/go-ethereum-master']
