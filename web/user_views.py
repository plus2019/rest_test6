#!/usr/bin/env python 
# -*- coding:utf-8 -*-
# !author:bruce chou
import time
import traceback
import uuid

import requests
from flask import request
from flask.json import jsonify

from config import config
from db.model import UserModel
from forms.user_form import UserForm
from libs.error import OperationError
from libs.tool import db_connect
from . import web
from .error_views import not_found

DB = config.bigcode_MYSQL_DB

GITHUB_API_USER_PREFIX = config.GITHUB_API_USER_PREFIX
GITHUB_TOKEN = config.GITHUB_TOKEN
API_HEADER = {
    'github': {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0',
               'Connection': 'close', 'Authorization': 'token ' + GITHUB_TOKEN},
}


def log(string):
    t = time.strftime(r"%Y-%m-%d-%H:%M:%S", time.localtime())
    print("[%s]%s" % (t, string))


def get_user_info(username, repo_source):
    flag = 0
    while True:
        try:
            if repo_source == 'github':
                url = GITHUB_API_USER_PREFIX + '/' + username
            #elif repo_source == 'gitlab':
            #   url = GITLAB_API_USER_PREFIX + '/?username=' + username
            else:
                log('输入的repoSource有误==>:' + repo_source)

            requests.adapters.DEFAULT_RETRIES = 5
            s = requests.session()
            s.keep_alive = False
            response = requests.get(url, timeout=15, headers=API_HEADER[repo_source])
            if response.status_code != 200:
                log('状态码: ' + str(response.status_code))
                return None
            json_data = response.json()
            log('headers: ' + str(response.headers))
        except Exception as e:
            log(e.__str__())
            traceback.print_exc()
            flag += 1
            if flag > 3:
                return None
        else:
            if repo_source == 'gitlab':
                json_data = json_data[0]
            if 'id' not in json_data:
                return None
            else:
                return json_data


@web.route('/user', methods=['GET'])
def get_userId():
    form = UserForm(request.args)
    if form.validate():
        username = form.username.data
        source = form.source.data
        try:
            session = db_connect(DB)
            query_ret = session.query(UserModel).filter(UserModel.username == username,
                                                        UserModel.source == source).first()
            data = dict()
            if query_ret is not None:
                field_list = ('uuid', 'username', 'id', 'source')
                for field in field_list:
                    data[field] = str(query_ret.__getattribute__(field))
            else:
                user_info = get_user_info(username, source)
                data['uuid'] = uuid.uuid1().__str__()
                data['id'] = user_info.get('id')
                data['username'] = username
                data['source'] = source
                # 将获取的数据插入到数据库中存储
                user = UserModel(uuid=data['uuid'], id=data['id'],
                                 username=data['username'], source=data['source'])
                session.add(user)
                # 不要忘记commit
                session.commit()
            session.close()
        except Exception as e:
            print(e)
            return not_found()
        else:
            return jsonify(data=data)
    else:
        raise OperationError('Invalid parameters')
