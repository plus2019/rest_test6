# encoding:utf-8

from flask import jsonify, request
from db.model import CommitModel, AccountauthorModel
from forms.commit_form import CommitForm, CommitIdForm
from libs.error import OperationError
from config import config
from libs.tool import db_connect, log
from web.error_views import not_found
from . import web
from sqlalchemy import func

DB = config.bigcode_MYSQL_DB
TIME_FIELD_LIST = ('commit_time',)


def send_msg(host, recv, msg):
    from kafka import KafkaProducer
    import json
    producer = KafkaProducer(bootstrap_servers=host, api_version=(0, 9))
    producer.send(recv, json.dumps(msg).encode())
    producer.close()


@web.route('/commit', methods=['GET'])
def get_commit():
    form = CommitForm(request.args)
    if form.validate():
        repo_id = form.repo_id.data
        page = form.page.data
        per_page = form.per_page.data
        is_whole = form.is_whole.data
        developer = form.developer.data
        start_time = form.start_time.data
        end_time = form.end_time.data
        try:
            session = db_connect(DB)
            if developer is '':
                if is_whole is True:
                    query_ret = session.query(CommitModel).filter(CommitModel.repo_id == repo_id,
                                                                  CommitModel.commit_time >= start_time,
                                                                  CommitModel.commit_time <= end_time). \
                        order_by(CommitModel.commit_time.desc()).all()
                else:
                    query_ret = session.query(CommitModel).filter(CommitModel.repo_id == repo_id,
                                                                  CommitModel.commit_time >= start_time,
                                                                  CommitModel.commit_time <= end_time). \
                        order_by(CommitModel.commit_time.desc()).limit(per_page).offset((page - 1) * per_page)
            else:
                if repo_id is '':
                    query_ret = session.query(CommitModel).filter(CommitModel.developer == developer,
                                                                  CommitModel.commit_time >= start_time,
                                                                  CommitModel.commit_time <= end_time). \
                        order_by(CommitModel.commit_time.desc()).all()
                else:
                    query_ret = session.query(CommitModel).filter(CommitModel.developer == developer,
                                                                  CommitModel.repo_id == repo_id,
                                                                  CommitModel.commit_time >= start_time,
                                                                  CommitModel.commit_time <= end_time). \
                        order_by(CommitModel.commit_time.desc()).all()

            data = []
            field_list = ('uuid', 'commit_id', 'message', 'developer', 'commit_time', 'repo_id', 'developer_email')
            for item in query_ret:
                dic = dict()
                for field in field_list:
                    if field in TIME_FIELD_LIST:
                        dic[field] = str(item.__getattribute__(field))
                    else:
                        dic[field] = item.__getattribute__(field)
                data.append(dic)
                del dic
            session.close()
        except Exception as e:
            print(e)
            return not_found()
        else:
            return jsonify(data=data)

    else:
        raise OperationError('Invalid parameters')


@web.route('/commit/one-day', methods=['GET'])
def get_commit_one_day():
    try:
        repo_id = request.args['repo_id']
        commit_time = request.args['commit_time'].replace('.', '-')
        session = db_connect(DB)
        query_ret = session.query(CommitModel).filter(CommitModel.repo_id == repo_id,
                                                      CommitModel.commit_time.like(commit_time + "%")).all()
        data = []
        field_list = ('uuid', 'commit_id', 'message', 'developer', 'commit_time', 'repo_id', 'developer_email')
        for item in query_ret:
            dic = dict()
            for field in field_list:
                if field in TIME_FIELD_LIST:
                    dic[field] = str(item.__getattribute__(field))
                else:
                    dic[field] = item.__getattribute__(field)
            data.append(dic)
            del dic
        session.close()
    except Exception as e:
        raise OperationError(e.__str__())
    else:
        if query_ret is None:
            raise OperationError('Invalid parameters')

        else:
            return jsonify(data=data)


@web.route('/commit/<commit_id>', methods=['GET'])
def get_one_commit(commit_id):
    form = CommitIdForm(data={'commit_id': commit_id})
    if form.validate():
        try:
            session = db_connect(DB)
            query_ret = session.query(CommitModel).filter(CommitModel.commit_id == commit_id).first()
            data = dict()
            field_list = ('uuid', 'commit_id', 'message', 'developer', 'commit_time', 'repo_id', 'developer_email')
            for field in field_list:
                if field in TIME_FIELD_LIST:
                    data[field] = str(query_ret.__getattribute__(field))
                else:
                    data[field] = query_ret.__getattribute__(field)
            session.close()
        except:
            return not_found()
        else:
            return jsonify(data=data)

    else:
        raise OperationError('Invalid parameters')


@web.route('/commit/developer-lists-by-commits', methods=['POST'])
def get_developer_email_by_commit():
    try:
        key_set = request.form.get('key_set').split(',')
        session = db_connect(DB)
        query_ret = session.query(CommitModel).filter(CommitModel.commit_id.in_(key_set)).all()
        dev_info = dict()
        data = dict()
        for ret, key in zip(query_ret, key_set):
            dev_info[key] = str(ret.__getattribute__('developer_email'))
        session.close()
    except Exception as e:
        raise OperationError(e.__str__())
    else:
        if query_ret is None:
            raise OperationError('Invalid parameters')

        else:
            data['status'] = 'Successful'
            data['data'] = dev_info
    return jsonify(data)


@web.route('/commit/commit-time', methods=['GET'])
def commit_time():
    try:
        commit_id = request.args['commit_id']
        repo_id = request.args['repo_id']
        session = db_connect(DB)
        query_ret = session.query(CommitModel).filter(CommitModel.commit_id == commit_id,
                                                      CommitModel.repo_id == repo_id).first()
        field_list = ('commit_time',)
        data = dict()
        for field in field_list:
            if field in TIME_FIELD_LIST:
                data[field] = str(query_ret.__getattribute__(field))
            else:
                data[field] = query_ret.__getattribute__(field)
        session.close()
    except Exception as e:
        raise OperationError(e.__str__())
    else:
        if query_ret is None:
            raise OperationError('Invalid parameters')

        else:
            data['status'] = 'Successful'
            return jsonify(data=data)


@web.route('/commit/latest-commit-time', methods=['GET'])
def get_latest_commit_time():
    try:
        repo_id = request.args['repo_id']
        session = db_connect(DB)
        query_ret = session.query(func.max(CommitModel.commit_time).label('commit_time')).filter(
            CommitModel.repo_id == repo_id).first()
        field_list = ('commit_time',)
        data = dict()
        for field in field_list:
            if field in TIME_FIELD_LIST:
                data[field] = str(query_ret.__getattribute__(field))
            else:
                data[field] = query_ret.__getattribute__(field)
        session.close()
    except Exception as e:
        raise OperationError(e.__str__())
    else:
        if query_ret == (None,):
            raise OperationError('Invalid parameters')

        else:
            data['status'] = 'Successful'
            return jsonify(data=data)


@web.route('/commit/distance-to-latest-commit', methods=['GET'])
def get_distance_to_latest_commit():
    try:
        repo_id = request.args['repo_id']
        commit_id = request.args['commit_id']
        session = db_connect(DB)
        query_commit_time = session.query(CommitModel.commit_time).filter(CommitModel.repo_id == repo_id,
                                                                          CommitModel.commit_id == commit_id).first()
        commit_time = str(query_commit_time.__getattribute__('commit_time'))
        query_ret = session.query(func.count('*').label('nums')).filter(CommitModel.repo_id == repo_id,
                                                                        CommitModel.commit_time > commit_time).first()
        data = dict()
        data['num'] = query_ret.__getattribute__('nums')
        session.close()
    except Exception as e:
        raise OperationError(e.__str__())
    else:
        if query_ret == (None,):
            raise OperationError('Invalid parameters')
            data['status'] = 'Failed'
            return jsonify(data=data)
        else:
            data['status'] = 'Successful'
            return jsonify(data=data)


@web.route('/commit/first-commit', methods=['GET'])
def get_first_commit():
    try:
        log('请求commit/first-commit接口')
        author = request.args['author']
        session = db_connect(DB)
        authorlist = session.query(AccountauthorModel.account_gitname).filter(AccountauthorModel.account_name == author).all()
        authorlist_final = []
        for aut in authorlist:
            authorname = aut.__getattribute__('account_gitname')
            authorlist_final.append(authorname)
        query_ret = session.query(CommitModel.repo_id, func.min(CommitModel.commit_time).label('first_commit_time')).filter(
            CommitModel.developer.in_(authorlist_final)).group_by('repo_id').order_by('first_commit_time').all()

        first_commit_time_set = set()
        repos_min_commit_list = []
        for item in query_ret:
            repo_id = item.__getattribute__('repo_id')
            first_commit_time = item.__getattribute__('first_commit_time')
            first_commit_time_set.add(first_commit_time)
            # repo_id_first_commit_time_dict的简写
            rifct = dict()
            rifct['repo_id'] = repo_id
            rifct['first_commit_time'] = str(first_commit_time)
            repos_min_commit_list.append(rifct)
        if first_commit_time_set:
            repos_summary = dict()
            repos_summary['first_commit_time_summary'] = str(min(first_commit_time_set))
            return_data = {
                'status': 'Successful',
                'repos_summary': repos_summary,
                'repos': repos_min_commit_list
            }
        else:
            return_data = {
                'status': 'Failed',
                'cause:': 'This author maybe not exist or has no commit!'
            }
        session.close()
    except Exception as e:
        log(e.__str__())
        raise OperationError(e.__str__())
    else:
        return jsonify(data=return_data)


@web.route('/commit/developer', methods=['GET'])
def get_developer():
    try:
        # repo_id = request.args['repo_id']
        session = db_connect(DB)
        query_ret = session.query(CommitModel.developer).filter().all()
        # query_ret = session.query(CommitModel.developer).filter(CommitModel.repo_id == repo_id).all()
        data = dict()
        data['developer'] = []
        for item in query_ret:
            developer = item.__getattribute__('developer')
            if developer not in data['developer']:
                data['developer'].append(developer)
        session.close()
    except Exception as e:
        raise OperationError(e.__str__())
    else:
        if query_ret == (None,):
            raise OperationError('Invalid parameters')

        else:
            data['status'] = 'Successful'
            return jsonify(data=data)

# @web.route('/commit/checkout', methods=['GET'])
# def checkout():
#     form = CheckoutForm(request.args)
#     if form.validate():
#         repo_id = form.repo_id.data
#         commit_id = form.commit_id.data
#         try:
#             repo_info = mysqlOperation.get_data_from_mysql(
#                 tablename = 'repository',
#                 params = {'uuid':repo_id},
#                 fields = ['local_addr']
#             )
#             local_addr = repo_info[0][0]
#             project_path = config.REPO_PATH + '/' +  local_addr
#             os.chdir(project_path)
#
#             os.system('git checkout ' + commit_id)
#         except:
#             raise OperationError('Internal error')
#         else:
#             message = {
#                 'status': 'Successful'
#             }
#             return jsonify(data=message)
#     else:
#         raise OperationError('Invalid parameters')


# @web.route('/commit/checkout-master/<repo_id>', methods=['GET'])
# def checkout_master(repo_id):
#     form = CheckoutMasterForm(data={'repo_id':repo_id})     #检查一下服务器上的代码是否一致
#     if form.validate():
#         repo_id = form.repo_id.data
#         try:
#             repo_info = mysqlOperation.get_data_from_mysql(
#                 tablename = 'repository',
#                 params = {'uuid':repo_id},
#                 fields = ['local_addr']
#             )
#             local_addr = repo_info[0][0]
#             project_path = config.REPO_PATH + '/' +  local_addr
#             os.chdir(project_path)
#
#             os.system('git checkout master')
#         except:
#             raise OperationError('Internal error')
#         else:
#             message = {
#                 'status': 'Successful'
#             }
#             return jsonify(data=message)
#     else:
#         raise OperationError(form.errors)

#
#
# @web.route('/commit/update/<repo_id>', methods=['GET'])      #暂时改成project_id
# def update_commit(repo_id):
#     try:
#         local_addr = mysqlOperation.get_data_from_mysql(
#             tablename='repository',
#             params={'uuid':repo_id},
#             fields=['local_addr']
#         )[0][0]
#         if len(local_addr) == 0:
#             raise OperationError('Invalid parameters')
#     except:
#         raise OperationError('Internal error')
#     else:
#         os.chdir(config.REPO_PATH + '/' + local_addr)
#         os.system('git checkout master')
#         os.system('git pull')
#         mysqlOperation.delete_from_mysql(
#             tablename='commit',
#             field='repo_id',
#             value=repo_id
#         )
#         msg = {'repoId':repo_id, 'projectId':'unknown', 'local_addr':local_addr}
#         send_msg(host=config.KAFKA_HOST['host-1'], recv=config.KAFKA_TOPIC['topic-3'], msg=msg)
#         message = {
#             'status': 'Successful'
#         }
#         return jsonify(data=message)
#
#
# @web.route('/commit/diff', methods=['GET'])
# def get_diff():
#     form = DiffForm(request.args)
#     if form.validate():
#         repo_id = form.repo_id.data
#         start = form.start.data
#         end = form.end.data
#         try:
#             local_addr = mysqlOperation.get_data_from_mysql(
#                 tablename='repository',
#                 params={'uuid': repo_id},
#                 fields=['local_addr']
#             )[0][0]
#             if len(local_addr) == 0:
#                 raise OperationError('Invalid parameters')
#         except:
#             raise OperationError('Internal error')
#         else:
#             os.chdir(config.REPO_PATH + '/' + local_addr)
#             os.system('git diff %s %s > diff.log' % (start, end))
#             with open(config.REPO_PATH + '/' + local_addr + '/diff.log', 'r') as f:
#                 data = f.read()
#             message = {
#                 'status': 'Successful',
#                 'diff':data
#             }
#             return jsonify(data=message)
