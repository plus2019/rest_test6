import json
import os
from redis import Redis

from flask import jsonify, request
from . import web
from libs.error import OperationError
from config.config import REPO_PATH
from config import config
from config.config import REPO_PATH
from libs.tool import from_path_get_branch, db_connect, get_max_repo_duplicate_count, deplicate_repo, log, \
    clean_working_space, reset_working_space, remove_to_update_workspace_from_redis, clone_repo
from db.model import RepositoryModel

KAFKA_TOPIC_COMPLETE_DOWNLOAD = 'UpdateCommit'
KAFKA_HOST = config.KAFKA_HOST
LOCALHOST = config.LOCALHOST
DB = config.bigcode_MYSQL_DB
DUPLICATE_UPPER_LIMIT = config.DUPLICATE_UPPER_LIMIT
GROUP = config.GROUP['group-1']
CLONE_REPO = config.CLONE_REPO


@web.route('/code/<file_path>/<line_number>', methods=['GET'])
def getcode(file_path, line_number):
    file_path = file_path+'/apache/hadoop-trunk/dev-support/findHangingTest.sh'
    # line_number = 1
    if int(line_number) < 1:
        return jsonify('')
    root_path = config.REPO_PATH + '/' + file_path
    if not os.path.exists(root_path):
        return jsonify('路径错误，请重试~')
    for cur_line_number, line in enumerate(open(root_path, 'rU')):
        if cur_line_number == int(line_number) - 1:
            return jsonify(line)
    return jsonify('')


@web.route('/code-service', methods=['GET'])
def get_root_path_multi():
    if request.args:
        repo_id = request.args['repo_id']
        commit_id = request.args.get('commit_id')
        try:
            session = db_connect(DB)
            query_ret = session.query(RepositoryModel.local_addr).filter(RepositoryModel.uuid == repo_id).first()
            if query_ret is None:
                message = {
                    'status': 'Failed',
                    'content': 'This repository does not exist!'
                }
                return jsonify(data=message)
            r = Redis(host=config.REDIS['host'], password=config.REDIS['password'], db=config.REDIS['db'])
            first_ele = r.lrange(repo_id, 0, 0)[0].decode()
            if first_ele == 'none':
                max_count = get_max_repo_duplicate_count(query_ret.local_addr)
                if max_count >= DUPLICATE_UPPER_LIMIT:
                    message = {
                        'status': 'Failed',
                        'content': 'This repository has no free working space.Please try again later!'
                    }
                    return jsonify(data=message)
                deplicate_repo(query_ret.local_addr, max_count)

            try:
                first_ele = r.lpop(repo_id).decode()
                # host_branch_index = host-branch-index是由这三个元素组成的(修改之后觉得host不存了)
                branch_index = first_ele.split('-')
                # log(first_ele)
                # log(branch_index)
                index = branch_index[-1]
                root_path = REPO_PATH + '/' + query_ret.local_addr + '_duplicate_xqb-' + index
                root_path = root_path.rstrip("\n")  # 去除path末尾的换行符
                repo_name = query_ret.local_addr.split('/')[-1] + '_duplicate_xqb-' + index
                os.chdir(root_path)
                #os.system('sudo chown -R %s ../%s/' % (GROUP, repo_name))  # 文件权限
                reset_working_space(root_path)
                if commit_id is None:
                    commit_id = get_repo_branch(repo_id)
                ret = os.system('git clean -d -fx')
                ret2 = os.system('git checkout %s' % commit_id)
                if ret != 0 and ret2 != 0:
                    message = {
                        'status': 'Failed',
                        'content': 'This commit has problem(maybe not exist) or checkout error！'
                    }
                    r.lpush(repo_id, first_ele)
                    # log('ret!=0 出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
                    return jsonify(data=message)
            except Exception as e:
                r.lpush(repo_id, first_ele)
                message = {
                    'status': 'Failed',
                    'content': 'Cannot get the repository  because:' + e.__str__()
                }
                # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
                return jsonify(data=message)
            session.close()
        except Exception as e:
            # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
            raise OperationError(e.__str__())
        else:
            message = {
                'status': 'Successful',
                'content': root_path
            }
            log('分配的工作区空间为：' + root_path)
            return jsonify(data=message)
    else:
        try:
            session = db_connect(DB)
            query_ret = session.query(RepositoryModel.uuid).filter().all()
            for item in query_ret:
                repo_id = item.__getattribute__('uuid')
                query_ret1 = session.query(RepositoryModel.local_addr).filter(RepositoryModel.uuid == repo_id).first()
                clone_repo(query_ret1.local_addr)

                try:
                    split = query_ret1.local_addr.split('/')
                    branch = get_repo_branch(repo_id)
                    user = split[1]
                    repo_name = query_ret1.local_addr.split('/')[-1]
                    clone_path = REPO_PATH + '/clone'
                    root_path = clone_path + '/' + user + '/' + repo_name
                    root_path = root_path.rstrip("\n")  # 去除path末尾的换行符
                    os.chdir(root_path)
                    os.system('sudo chown -R %s ../%s/' % (GROUP, repo_name))  # 文件权限
                    reset_working_space(root_path)
                    ret = os.system('git clean -d -fx')
                    ret2 = os.system('git checkout %s' % branch)
                    # ret3 = os.system('git pull')
                    if ret != 0 and ret2 != 0:
                        msg = repo_name + ': 生成工作区失败！'
                        message = {
                            'status': 'Failed',
                            'content': msg
                        }
                        return jsonify(data=message)
                except Exception as e:
                    msg1 = 'Cannot get repository : ' + repo_name + 'because:'
                    message = {
                        'status': 'Failed',
                        'content': msg1 + e.__str__()
                    }
                    # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
                    return jsonify(data=message)
            session.close()
            for repo_addr in CLONE_REPO:
                try:
                    split = repo_addr.split('/')
                    # branch = get_repo_branch(repo_id)
                    branch = repo_addr.split('-')[-1]
                    user = split[1]
                    repo_name = repo_addr.split('/')[-1]
                    clone_path = REPO_PATH + '/clone'
                    root_path = clone_path + '/' + user + '/' + repo_name
                    root_path = root_path.rstrip("\n")  # 去除path末尾的换行符
                    os.chdir(root_path)
                    os.system('sudo chown -R %s ../%s/' % (GROUP, repo_name))  # 文件权限
                    reset_working_space(root_path)
                    ret = os.system('git clean -d -fx')
                    ret2 = os.system('git checkout %s' % branch)
                    # ret3 = os.system('git pull')
                    if ret != 0 and ret2 != 0:
                        msg = repo_name + ': 生成工作区失败！'
                        message = {
                            'status': 'Failed',
                            'content': msg
                        }
                        return jsonify(data=message)
                except Exception as e:
                    msg1 = 'Cannot get repository : ' + repo_name + 'because:'
                    message = {
                        'status': 'Failed',
                        'content': msg1 + e.__str__()
                    }
                    # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
                    return jsonify(data=message)
        except Exception as e:
            # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
            raise OperationError(e.__str__())
        else:
            message = {
                'status': 'Successful',
                'content': clone_path
            }
            log('分配的工作区空间为：' + clone_path)
            return jsonify(data=message)


@web.route('/code-service/free', methods=['GET'])
def free_resource_multi():
    try:
        repo_id = request.args['repo_id']
        path = request.args['path']

        session = db_connect(DB)
        query_ret = session.query(RepositoryModel.url, RepositoryModel.local_addr).filter(
            RepositoryModel.uuid == repo_id).first()
        r = Redis(host=config.REDIS['host'], password=config.REDIS['password'], db=config.REDIS['db'])
        # path合法性判断
        # /home/fdse/user/issueTracker/repo/github/upsnailNostalgia/lrfRepos-master_duplicate_fdse-0
        # master-0
        path = path.rstrip("\n")  # 去除path末尾的换行符
        path_split = path.split('/')
        # host = from_path_get_host(path)
        branch = from_path_get_branch(path, query_ret.url.split('/')[-1])
        index = path_split[-1].split('-')[-1]
        item = '%s-%s' % (branch, index)
        key = repo_id
        max_count = get_max_repo_duplicate_count(query_ret.local_addr)

        if max_count <= int(index):
            message = {
                'status': 'Failed',
                'content': 'This repository is invalid!'
            }
            return jsonify(data=message)
        for ele in r.lrange(key, 0, -1):
            if item == ele.decode():
                message = {
                    'status': 'Failed',
                    'content': 'This repository has been freed already!'
                }
                remove_to_update_workspace_from_redis(repo_id, path)
                return jsonify(data=message)
        r.lpush(key, item)
        session.close()
        # 访问控制

        # 将工作区清理干净（可能由于上层处理产生了target等文件）
        # master_path = path.split('_duplicate_fdse-')[-2]
        # remove_and_copy(master_path, path)
        reset_working_space(path)
        clean_working_space(path)
        # 此处需要判断当前工作区的主本在使用过程中是否被更新过了，如果更新过了，会在redis中存此工作区（副本）的信息，然后需要更新
        remove_to_update_workspace_from_redis(repo_id, path)

    except Exception as e:
        message = {
            'status': 'Failed',
            'content': 'Freeing the repository failed because:' + e.__str__()
        }
        log('repo_id为：' + repo_id)
        log('path为：' + path)
        return jsonify(data=message)
    else:
        message = {
            'status': 'Successful',
            'content': 'Freeing the repository successfully!'
        }
        log('释放的工作区空间为：' + path)
        return jsonify(data=message)

#
# @web.route('/code-service', methods=['GET'])
# def get_clone_path():
#     try:
#         session = db_connect(DB)
#         query_ret = session.query(RepositoryModel.uuid).filter().all()
#         for item in query_ret:
#             repo_id = item.__getattribute__('uuid')
#             query_ret1 = session.query(RepositoryModel.local_addr).filter(RepositoryModel.uuid == repo_id).first()
#             clone_repo(query_ret1.local_addr)
#
#             try:
#                 split = query_ret1.local_addr.split('/')
#                 branch = get_repo_branch(repo_id)
#                 user = split[1]
#                 repo_name = query_ret1.local_addr.split('/')[-1]
#                 clone_path = REPO_PATH + '/clone'
#                 root_path = clone_path + '/' + user + '/' + repo_name
#                 root_path = root_path.rstrip("\n")  # 去除path末尾的换行符
#                 os.chdir(root_path)
#                 os.system('sudo chown -R %s ../%s/' % (GROUP, repo_name))  # 文件权限
#                 reset_working_space(root_path)
#                 ret = os.system('git clean -d -fx')
#                 ret2 = os.system('git checkout %s' % branch)
#                 # ret3 = os.system('git pull')
#                 if ret != 0 and ret2 != 0:
#                     msg = repo_name + ': 生成工作区失败！'
#                     message = {
#                         'status': 'Failed',
#                         'content': msg
#                     }
#                     return jsonify(data=message)
#             except Exception as e:
#                 msg1 = 'Cannot get repository : ' + repo_name + 'because:'
#                 message = {
#                     'status': 'Failed',
#                     'content': msg1 + e.__str__()
#                 }
#                 # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
#                 return jsonify(data=message)
#         session.close()
#     except Exception as e:
#         # log('出错的repo_id为：' + repo_id + '    出错的commit_id为：' + commit_id)
#         raise OperationError(e.__str__())
#     else:
#         message = {
#             'status': 'Successful',
#             'content': clone_path
#         }
#         log('分配的工作区空间为：' + clone_path)
#         return jsonify(data=message)


def get_repo_branch(repo_id):
    try:
        session = db_connect(DB)
        ret = session.query(RepositoryModel.branch).filter(
            RepositoryModel.uuid == repo_id).first()
        session.close()
    except Exception:
        raise OperationError('Invalid query!')
    else:
        return ret.__getattribute__('branch')

