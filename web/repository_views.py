from datetime import datetime

from flask import jsonify
from flask import request
from sqlalchemy import text, func

from . import web
from libs.error import OperationError
from config import config
from libs.tool import db_connect, file_count, log
from db.model import RepositoryModel, CommitModel
from service.Service import update_commit, add_commit
from libs.mysqlOperation import delete_from_mysql
from config.config import REPO_PATH
import os


DB = config.bigcode_MYSQL_DB  # ########################这里配置需要改
DEFAULT_FIELD_LIST = ('uuid', 'local_addr', 'is_private', 'description')
REPO_PREFIX = config.REPO_PATH


@web.route('/repository', methods=['POST'])
def query_repo_info():
    query = request.form.get('query')
    field_list = DEFAULT_FIELD_LIST if request.form.get('field_list') is None else request.form.get('field_list').split(
        ',')  # 默认参数
    page = 1 if request.form.get('page') is None else int(request.form.get('page'))  # 默认参数
    per_page = 1000 if request.form.get('per_page') is None else int(request.form.get('per_page'))  # 默认参数,
    try:
        session = db_connect(DB)
        count = session.query(RepositoryModel).filter(text(query)).count()
        ret = session.query(RepositoryModel).filter(text(query)).limit(per_page).offset((page - 1) * per_page)
    except Exception as e:
        # raise OperationError('Invalid query')
        raise OperationError(e.__str__())  # 调试模式
    else:
        session.close()
        data = []
        for item in ret:
            dic = dict()
            for field in field_list:
                try:
                    if field == 'local_addr':
                        dic[field] = REPO_PREFIX + '/' + item.__getattribute__(field)
                    else:
                        dic[field] = item.__getattribute__(field)
                except Exception:
                    raise OperationError('Invalid field list')
            data.append(dic)
        return_data = {
            'count': count,
            'data': data
        }
        return jsonify(return_data)


@web.route('/repository/<repo_id>', methods=['GET'])
def get_repo_info(repo_id):
    try:
        session = db_connect(DB)
        ret = session.query(RepositoryModel).filter(RepositoryModel.uuid == repo_id).first()
    except Exception:
        raise OperationError('Invalid query')
        # raise OperationError(e.__str__()) # 调试模式
    else:
        session.close()
        field_list = ('uuid', 'repository_id', 'language', 'description', 'url', 'local_addr', 'is_private', 'branch')
        data = dict()
        for field in field_list:
            try:
                if field == 'local_addr':
                    data[field] = REPO_PREFIX + '/' + ret.__getattribute__(field)
                else:
                    data[field] = ret.__getattribute__(field)
            except Exception:
                raise OperationError('Invalid field list')

        return jsonify(data=data)


@web.route('/repository/repository_year', methods=['GET'])
def get_repo_year():
    try:
        repo_id = request.args['repo_id']
        end_date = request.args['end_date']
        session = db_connect(DB)
        ret = session.query(func.min(CommitModel.commit_time).label('commit_time')).filter(
            CommitModel.repo_id == repo_id).first()
    except Exception:
        raise OperationError('Invalid query!')
    else:
        session.close()
        data = dict()
        try:
            data['commit_time'] = (datetime.strptime(end_date, '%Y-%m-%d') - ret.__getattribute__('commit_time')).days
        except Exception:
            raise OperationError('Invalid field list or datetime convert error!')

        return jsonify(data=data)


@web.route('/repository/number-of-files', methods=['GET'])
def get_number_of_files():
    try:
        repo_id = request.args['repo_id']
        session = db_connect(DB)
        ret = session.query(RepositoryModel.local_addr).filter(RepositoryModel.uuid == repo_id).first()
    except Exception:
        raise OperationError('Invalid query!')
    else:
        session.close()
        try:
            local_addr = ret.__getattribute__('local_addr')
            new_path = REPO_PREFIX + '/' + local_addr
            data = file_count(new_path)
        except Exception:
            raise OperationError('Invalid field list')

        return jsonify(data=data)


@web.route('/repository/<repo_id>', methods=['PUT'])
def update_repository(repo_id):
    try:
        session = db_connect(DB)
        ret = session.query(RepositoryModel).filter(RepositoryModel.uuid == repo_id).first()
    except Exception:
        raise OperationError('Invalid query')
    else:
        session.close()
        # field_list = ('uuid', 'repository_id', 'language', 'description', 'url', 'local_addr', 'is_private', 'branch')
        data = []
        try:
            uuid = ret.__getattribute__('uuid')
            local_addr = ret.__getattribute__('local_addr')
        except Exception:
            message = {
                'data': 'The repository does not exist!',
                'msg': 'failed',
                'code': 400
            }
            log('更新repo 失败！')
            return jsonify(message)
        else:
            item = [uuid, local_addr]
            data = update_commit(item)
            length = data[0]
            max_index = data[1]
            log('更新之后的commit数量为：' + str(length) + '\t更新之前的commit数量为：' + str(max_index))
            if length > max_index:
                log('该repo此次update存在新的commit，uuid为' + uuid)
                msg = {
                    'repoId': uuid,
                    'local_addr': item[1],
                    'max_index': max_index,
                    'flag': 'not first added and existed'
                }
                add_commit(msg)  # 向commit表中添加更新的commit
                message = {
                    'data': 'Updated',
                    'msg': 'success',
                    'code': 200
                }
                log('已更新repo： ' + uuid)
                return jsonify(message)
            else:
                message = {
                    'data': 'NoUpdated',
                    'msg': 'success',
                    'code': 200
                }
                return jsonify(message)


@web.route('/DELETE/base/<repo_id>', methods=['GET'])
def delete_repo(repo_id):
    try:
        session = db_connect(DB)
        ret = session.query(RepositoryModel.local_addr).filter(RepositoryModel.uuid == repo_id).first()
    except Exception:
        raise OperationError('Invalid query')
        # raise OperationError(e.__str__()) # 调试模式
    else:
        session.close()
        if ret is None:
            message = {
                'status': 'Failed',
                'content': 'This repository does not exist!'
            }
            return jsonify(data=message)
        try:
            delete_from_mysql(tablename='commit', field='repo_id', value=repo_id)  # 删除commit表
            split = ret.local_addr.split('/')
            repo_type = split[0]
            user = split[1]
            root_path = REPO_PATH + '/' + repo_type
            root_path = root_path.rstrip("\n")  # 去除path末尾的换行符
            os.chdir(root_path)
            if os.path.exists(root_path + '/' + user):   # 判断代码库是否存在
                os.system('rm -rf %s' % user)  # 删除代码库
            delete_from_mysql(tablename='repository', field='uuid', value=repo_id)  # 删除repository表
        except Exception as e:
            log(e.__str__())
            message = {
                'status': 'Failed',
                'content': 'null'
            }
            log('Delete repo: ' + repo_id + ' failed!')
            return jsonify(message)
        else:
            message = {
                'status': 'Successful',
                'content': 'null'
            }
            log('Delete repo: ' + repo_id + ', addr: ' + ret.local_addr + ' succeed!')
            return jsonify(message)
