# #!/usr/bin/env python
# # -*- coding:utf-8 -*-
# # !author:bruce chou
# import traceback
#
# from flask import jsonify, request
# from redis import Redis
#
# import web
# from config import config
# from db.model import RepositoryModel
# from libs.tool import db_connect, log
#
# DB = config.ISSUE_TRACKER_MYSQL_DB
#
#
# @web.route('/workspace/names', methods=['GET'])
# def get_workspace_names():
#     page = 1 if request.form.get('page') is None else int(request.form.get('page'))
#     per_page = 100 if request.form.get('per_page') is None else int(request.form.get('per_page'))
#     try:
#         r = Redis(host=config.REDIS['host'], password=config.REDIS['password'], db=config.REDIS['db'])
#         repo_uuids = r.keys()
#         session = db_connect(DB)
#         data = {}
#         cur_index = 0
#         start_index = (page - 1) * per_page
#         end_index = page * per_page - 1
#         for repo_uuid in repo_uuids:
#             if cur_index >= start_index and end_index <= end_index:
#                 query_ret = session.query(RepositoryModel).filter(RepositoryModel.uuid == repo_uuid).first()
#                 # name也就是repository表中的url
#                 name = str(query_ret.__getattribute__('url'))
#                 data[repo_uuid] = name
#             elif cur_index > end_index:
#                 break
#             cur_index += 1
#         session.close()
#     except Exception as e:
#         log(e.__str__())
#         traceback.print_exc()
#     else:
#         return jsonify(data=data)
#
#
# @web.route('/workspace/remaining', methods=['GET'])
# def get_remaining_workspace():
#
