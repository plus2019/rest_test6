# coding:utf-8

import json
from kafka import KafkaProducer
import uuid
from datetime import datetime
import pytz
import time
import os

import pymysql

import subprocess
from redis import Redis
from config import config

REPO_PATH = config.REPO_PATH
KAFKA_HOST = config.KAFKA_HOST['host-1']
KAFKA_TOPIC_DOWNLOADED = config.KAFKA_TOPIC['topic-4']
KAFKA_TOPIC_UPDATED = config.KAFKA_TOPIC['topic-5']
REDIS = config.REDIS
GROUP = config.GROUP['group-1']


def local_to_utc(time_str, utc_format='%a %b %d %H:%M:%S %Y %z'):
    if int(time_str[-5:]) // 100 >= 0:
        timezone = pytz.timezone('Etc/GMT' + str(-int(time_str[-5:]) // 100))
    else:
        timezone = pytz.timezone('Etc/GMT+' + str(-int(time_str[-5:]) // 100))
    local_format = "%Y-%m-%d %H:%M:%S"
    utc_dt = datetime.strptime(time_str, utc_format)
    local_dt = utc_dt.replace(tzinfo=timezone).astimezone(pytz.utc)
    return local_dt.strftime(local_format)


def log(string):
    t = time.strftime(r"%Y-%m-%d-%H:%M:%S", time.localtime())
    print("[%s]%s%s" % (t, " Add Commit : ", string))


def create_commit_log(path):
    import os
    os.chdir(path)
    os.system('git log --pretty=format:"%H|++*淦*++|%an|++*淦*++|%ad|++*淦*++|%ae|++*淦*++|%s|++*淦*++|%P" > commit_log.log')


def get_single_commit_info(path, parent_commit):
    import os
    import subprocess
    os.chdir(path)
    batcmd = 'git log --pretty=format:"%H|++*淦*++|%an|++*淦*++|%ad|++*淦*++|%ae|++*淦*++|%s|++*淦*++|%P" | grep ^' + parent_commit
    mess = subprocess.check_output(batcmd, shell=True, encoding='utf-8')
    return mess.split('|++*淦*++|')


# 该函数将single_commit_info中的各个对应信息赋给前面的参数
def collect_commit_info(uuids, self_index, commit_sha, developer, commit_time, developer_email, message,
                        parent_commits, single_commit_info):
    uuids.append(uuid.uuid1().__str__())
    self_index.append(-1)
    commit_sha.append(single_commit_info[0])
    developer.append(single_commit_info[1])
    commit_time.append(local_to_utc(single_commit_info[2]))
    developer_email.append(single_commit_info[3])
    message.append(single_commit_info[4])
    parent_commits_list = single_commit_info[5].replace('\n', '').split(' ')
    parent_commits.append(parent_commits_list.__str__())


def get_commit_info(new_path, max_index, repo_id, flag):
    path = new_path + '/commit_log.log'
    with open(path, 'r', encoding='UTF-8') as f:
        length = f.readlines().__len__()
    with open(path, 'r', encoding='UTF-8') as f:
        commit_list = []
        uuids = []
        commit_sha = []
        developer = []
        commit_time = []
        developer_email = []
        message = []
        self_index = []
        parent_commits = []

        tmp = ''
        sur_line = 0  # 统计多余出来的行数
        for line in f.readlines():
            if line.split('|++*淦*++|').__len__() != 6:
                log('line:' + line)
                tmp += line
                sur_line += 1
                if tmp.split('|++*淦*++|').__len__() == 6:
                    commit_list.append(tmp.split('|++*淦*++|'))
                    tmp = ''
                    sur_line -= 1
            else:
                commit_list.append(line.split('|++*淦*++|'))
        # 存储之前mysql中没记录的具有嫌疑的parent_commit
        parent_commits_set = set()
        length -= sur_line
        for index in range(0, length - max_index):
            if flag == 'not first added and existed':
                # 首先得判断该commit是否出现在mysql中（可能某个commit在本地git commit了，但是没有push到github，然后我们git clone下来了）
                if is_commit_existed(tablename='commit', commit_id=commit_list[index][0],
                                     repo_id=repo_id) is True:
                    continue
            # 如果该commit不在commit表里存在，需要将其加进commit表
            collect_commit_info(uuids, self_index, commit_sha, developer, commit_time, developer_email, message,
                                parent_commits, commit_list[index])
            parent_commits_list = commit_list[index][5].replace('\n', '').split(' ')
            for parent_commit in parent_commits_list:
                if parent_commit != '':
                    parent_commits_set.add(parent_commit)
        for parent_commit in parent_commits_set:
            if parent_commit not in commit_sha and is_commit_existed(tablename='commit',
                                                                     commit_id=parent_commit,
                                                                     repo_id=repo_id) is False:
                single_commit_info = get_single_commit_info(new_path, parent_commit)
                collect_commit_info(uuids, self_index, commit_sha, developer, commit_time, developer_email, message,
                                    parent_commits, single_commit_info)
        dic = dict()
        dic['uuids'] = uuids
        dic['message'] = message
        dic['commit_id'] = commit_sha
        dic['commit_time'] = commit_time
        dic['developer_email'] = developer_email
        dic['developer'] = developer
        dic['self_index'] = self_index
        dic['parent_commit'] = parent_commits

        return dic


def insert_into_mysql(tablename, params={}, mode='single'):
    conn = get_connection()
    sql = "insert into %s " % tablename
    keys = params.keys()
    sql += "(`" + "`,`".join(keys) + "`)"  # 字段组合
    values = list(params.values())  # 值组合，由元组转换为数组
    sql += " values (%s)" % ','.join(['%s'] * len(values))  # 配置相应的占位符
    cur = conn.cursor()
    if mode == 'single':
        cur.execute(sql, values)
        conn.commit()
        cur.close()
        conn.close()
    elif mode == 'multiple':
        insert_items = []
        flag = 0
        index = 0
        while True:
            temp = []
            for key in keys:
                if index == len(params[key]):
                    flag = 1
                    break
                temp.append(params[key][index])
            if flag == 1:
                break
            insert_items.append(temp)
            index += 1

        cnt = 0
        for item in insert_items:
            cur.execute(sql, item)
            cnt += 1
            if cnt % 30 == 0:
                conn.commit()
        conn.commit()
        cur.close()
        conn.close()
    else:
        raise Exception


def delete_from_mysql(tablename, field='uuid', value=''):
    conn = get_connection()
    sql = "delete from %s " % tablename
    sql += " where %s = '%s' " % (field, value)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    cur.close()
    conn.close()


def update_mysql(tablename, params={}, pk='uuid'):
    conn = get_connection()
    cur = conn.cursor()
    sql = "update %s set " % tablename
    keys = params.keys()
    for key in keys:  # 字段与占位符拼接
        if key != pk:
            sql += "`" + key + "` = %(" + key + ")s,"
    sql = sql[:-1]  # 去掉最后一个逗号
    # sql += " where uuid = %(uuid)s "                 #只支持按主键进行修改
    sql += " where %s = " % pk + "%" + "(%s)s" % pk
    cur.execute(sql, params)
    conn.commit()
    cur.close()
    conn.close()


def get_data_from_mysql(tablename=None, params={}, fields=[], order_field=None, order_by='desc', start=None, num=None,
                        sql=None):
    order = ''
    limit = ''

    if order_field is not None:
        order = ' order by ' + order_field + ' ' + order_by

    if start is not None and num is not None:
        limit = ' limit ' + str(start) + ',' + str(num)

    conn = get_connection()
    if sql is not None:
        cur = conn.cursor()
        cur.execute(sql)
        ret = cur.fetchall()
        return ret
    else:
        sql = "select %s from %s " % ('*' if len(fields) == 0 else ','.join(fields), tablename)
        keys = params.keys()
        where = ""
        ps = []
        values = []
        if len(keys) > 0:  # 存在查询条件时，以与方式组合
            for key in keys:
                ps.append(key + " =%s ")
                values.append(params[key])
            where += ' where ' + ' and '.join(ps)
        cur = conn.cursor()
        cur.execute(sql + where + order + limit, values)
        ret = cur.fetchall()
        cur.close()
        conn.close()
        return ret


def is_commit_existed(tablename='commit', commit_id='', repo_id=''):
    if commit_id == '' or repo_id == '':
        return False
    conn = get_connection()
    sql = "select * from %s where repo_id = '%s' and commit_id = '%s' " % (tablename, repo_id, commit_id)
    cur = conn.cursor()
    cur.execute(sql)
    cur.fetchall()
    if cur.rowcount > 0:
        conn.close()
        return True
    else:
        conn.close()
        return False


def get_latest_commit(tablename='commit', repo_id=''):
    if repo_id == '':
        return False
    conn = get_connection()
    sql = "select max(commit_time) as commit_time from %s where repo_id = '%s' " % (tablename, repo_id)
    cur = conn.cursor()
    cur.execute(sql)
    ret = cur.fetchone()
    conn.close()
    for latest_commit_time in ret:
        return latest_commit_time


def get_connection():
    conn = pymysql.connect(
        host=config.ISSUE_TRACKER_MYSQL_DB['host'],
        db=config.ISSUE_TRACKER_MYSQL_DB['db'],
        user=config.ISSUE_TRACKER_MYSQL_DB['user'],
        passwd=config.ISSUE_TRACKER_MYSQL_DB['passwd'],
        charset=config.ISSUE_TRACKER_MYSQL_DB['charset'],
        port=int(config.ISSUE_TRACKER_MYSQL_DB['port'])
    )
    return conn


# 新添代码库或代码库更新时，向commit表中更新commit信息
def add_commit(add_msg):
    log(add_msg)
    msg_list = []
    repo_id = add_msg['repoId']
    local_addr = add_msg['local_addr']
    max_index = add_msg['max_index']
    flag = add_msg['flag']
    producer = KafkaProducer(bootstrap_servers=KAFKA_HOST, api_version=(0, 9))
    try:
        if flag == 'first added and not existed' or flag == 'not first added and existed':
            new_path = REPO_PATH + '/' + local_addr
            create_commit_log(new_path)
            commit_info = get_commit_info(new_path, max_index, repo_id, flag)
            insert_into_mysql(
                tablename='commit',
                params={
                    'uuid': commit_info['uuids'],
                    'commit_id': commit_info['commit_id'],
                    'message': commit_info['message'],
                    'commit_time': commit_info['commit_time'],
                    'repo_id': [repo_id] * len(commit_info['uuids']),
                    'developer': commit_info['developer'],
                    'developer_email': commit_info['developer_email'],
                    'self_index': commit_info['self_index'],
                    'parent_commit': commit_info['parent_commit']
                },
                mode='multiple'
            )

            if flag == 'first added and not existed':
                msg = {
                    'repoId': repo_id,
                    'branch': local_addr.split('-')[-1],
                    'isUpdate': False
                }
                producer.send(KAFKA_TOPIC_DOWNLOADED, json.dumps(msg).encode())
                producer.close()
                commit_info.clear()

            if flag == 'not first added and existed':
                msg = {
                    'repoId': repo_id,
                    'branch': local_addr.split('-')[-1],
                    'isUpdate': True
                }
                producer.send(KAFKA_TOPIC_UPDATED, json.dumps(msg).encode())
                producer.close()
                msg_list.clear()
                commit_info.clear()
        else:
            msg = {
                'repoId': repo_id,
                'branch': local_addr.split('-')[-1],
                'isUpdate': True
            }
            producer.send(KAFKA_TOPIC_UPDATED, json.dumps(msg).encode())
            producer.close()
            msg_list.clear()
    except Exception as e:
        log(e.__str__())


def get_max_repo_duplicate_count(local_addr):
    path = REPO_PATH + '/' + local_addr
    os.chdir(path)
    os.chdir('..')
    repo_name = local_addr.split('/')[-1]
    # 统计备份的repo个数
    batcmd = 'ls | grep ^' + repo_name + '_duplicate_fdse- | wc -l'
    mess = subprocess.check_output(batcmd, shell=True, encoding='utf-8')
    return int(mess)


def get_duplicate_indexes_from_redis(repo_id):
    try:
        r = Redis(host=REDIS['host'], password=REDIS['password'], db=REDIS['db'])
        duplicate_info_set = set()
        for value in r.lrange(repo_id, 0, -2):
            duplicate_info_set.add(int(value.decode().split('-')[-1]))
        return duplicate_info_set
    except Exception as e:
        log(e.__str__())


def add_to_be_freed_workspace(uuid, target_dir):
    try:
        r = Redis(host=REDIS['host'], password=REDIS['password'], db=REDIS['db'])
        r.set(uuid + '-' + target_dir, 1)
    except Exception as e:
        log(e.__str__())


def update_commit(item):
    try:
        uuid = item[0]
        log('uuid为：' + uuid)
        branch = item[1].split('-')[-1]
        split = item[1].split('/')
        user = split[1]
        repo_type = split[0]
        repo_name = split[-1]
        os.chdir(REPO_PATH + '/' + item[1])
        os.system('sudo chown -R %s ../%s/' % (GROUP, repo_name))  # 文件权限
        os.system('git reset --hard')  # 清空暂存区、工作区
        os.system('git checkout %s' % branch)
        os.system('git pull')
        os.system('git log --pretty=format:"%H" > check.log')
        with open(REPO_PATH + '/' + item[1] + '/check.log', 'r', encoding='UTF-8') as f:
            length = f.readlines().__len__()

        max_index = get_data_from_mysql(
            sql='select count(*) from commit where repo_id = "%s"' % uuid
        )[0][0]

        max_duplicate_num = get_max_repo_duplicate_count(item[1])
        duplicate_indexes_set = get_duplicate_indexes_from_redis(uuid)
        for i in range(max_duplicate_num):
            target_dir = REPO_PATH + '/%s/%s/%s_duplicate_fdse-%s' % (repo_type, user, repo_name, str(i))  # 之后要加入分支名
            if i in duplicate_indexes_set:
                # 项目备份名:<repo_name>_duplicate_fdse-<index>
                os.chdir(target_dir)
                os.system(
                    'sudo chown -R %s ../%s_duplicate_fdse-%s/' % (GROUP, repo_name, str(i)))  # 文件权限
                os.system('git reset --hard')  # 清空暂存区、工作区
                os.system('git checkout %s' % branch)
                os.system('git pull')
            else:
                add_to_be_freed_workspace(uuid, target_dir)

        data = [length, max_index, uuid]
        return data

    except Exception as e:
        log(e.__str__())
