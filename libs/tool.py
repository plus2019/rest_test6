import os
import subprocess

from redis import Redis
from config import config
from config.config import REPO_PATH
from libs.mysqlOperation import get_repository_uuid

REDIS_HOST = config.REDIS['host']
REDIS_DB = config.REDIS['db']
REDIS_PASSWORD = config.REDIS['password']


def calculate_start(page, per_page):
    return (page - 1) * per_page + 1


def db_connect(db_config):
    db_url = 'mysql+mysqlconnector://%s:%s@%s:%s/%s?charset=%s' % (
        db_config['user'],
        db_config['passwd'],
        db_config['host'],
        db_config['port'],
        db_config['db'],
        db_config['charset']
    )
    from sqlalchemy import create_engine

    engine = create_engine(db_url)

    from sqlalchemy.orm import sessionmaker

    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def log(string):
    import time
    t = time.strftime(r"%Y-%m-%d-%H:%M:%S", time.localtime())
    print("[%s]%s" % (t, string))


def from_path_get_branch(path, repo_name):
    import re
    branch_pattern = '/%s-([^/]{1,})_duplicate_fdse-[0-9]' % repo_name
    ret = re.findall(branch_pattern, path)
    if len(ret) == 0:
        raise Exception
    else:
        return ret[0]


def from_path_get_host(path):
    import re
    host_pattern = 'repo/([0-9\.]{1,})/'
    ret = re.findall(host_pattern, path)
    if len(ret) == 0:
        return config.LOCALHOST
    else:
        return ret[0]


def get_max_repo_duplicate_count(local_addr):
    path = REPO_PATH + '/' + local_addr
    os.chdir(path)
    os.chdir('..')
    repo_name = local_addr.split('/')[-1]
    # 统计备份的repo个数
    batcmd = 'ls | grep ^' + repo_name + '_duplicate_fdse- | wc -l'
    mess = subprocess.check_output(batcmd, shell=True, encoding='utf-8')
    return int(mess)


# code-service申请工作区，可以动态地备份repository，并将对应的branch-index存到redis
def deplicate_repo(local_addr, count):
    r = Redis(host=REDIS_HOST, db=REDIS_DB, password=REDIS_PASSWORD)
    os.chdir(REPO_PATH + '/' + local_addr)
    os.chdir('..')
    repo_name = local_addr.split('/')[-1]
    ret = os.system('cp -r %s %s' % (repo_name, repo_name + '_duplicate_fdse-' + str(count)))
    # 备份文件名：<repo_name>_duplicate_fdse-<index>  repo_name: <repo>-<branch>
    if ret != 0:
        raise Exception('生成副本失败！')
    else:
        branch = local_addr.split('-')[-1]
        r.lpush(get_repository_uuid(local_addr)[0], "%s-%s" % (branch, str(count)))


def remove_and_copy(master_path, path):
    os.chdir(path)
    os.chdir('..')
    ret_remove = os.system('rm -rf %s' % path)
    if ret_remove != 0:
        raise Exception('删除之前工作区 ' + path + ' 失败！')
    else:
        ret_copy = os.system('cp -r %s %s' % (master_path, path))
        if ret_copy != 0:
            raise Exception('从主本复制到副本失败！')


def clean_working_space(path):
    os.chdir(path)
    ret = os.system('git clean -df')
    if ret != 0:
        raise Exception('清理工作区失败！path为：' + path)


def reset_working_space(path):
    os.chdir(path)
    ret = os.system('git reset --hard')
    if ret != 0:
        raise Exception('git reset --hard失败！path为：' + path)
    else:
        log('git reset --hard 成功！')


def remove_to_update_workspace_from_redis(uuid, target_dir):
    if to_update_workspace_is_exists(uuid, target_dir) == 1:
        if update_workspace(target_dir) is True:
            remove_from_redis(uuid + '-' + target_dir)
        else:
            log('更新工作区空间失败！')


def to_update_workspace_is_exists(uuid, target_dir):
    r = Redis(host=REDIS_HOST, db=REDIS_DB, password=REDIS_PASSWORD)
    return r.exists(uuid + '-' + target_dir)


def update_workspace(target_dir):
    try:
        # 此处如果使用git pull会造成，当前副本工作空间的版本比主本超前（因为在主本更新 到 副本资源得到释放这个时间段中，远端又有新的commit提交了）
        # 所以需要使用直接copy主本到副本==================================================
        # # 项目备份名:<repo_name>_duplicate_fdse-<index>
        # os.chdir(target_dir)
        # branch = target_dir.split('_duplicate_fdse-')[0].split('-')[-1]
        # os.system('git checkout %s' % branch)
        # os.system('git pull')
        os.chdir(target_dir)
        os.chdir('../')
        master_dir = target_dir.split('_duplicate_fdse-')[0]
        return os.system('sudo cp -R ' + master_dir + '/. ' + target_dir + '/') == 0
    except Exception as e:
        log(e.__str__())
        return False


def remove_from_redis(key):
    r = Redis(host=REDIS_HOST, db=REDIS_DB, password=REDIS_PASSWORD)
    return r.delete(key)


# 文件统计
def file_count(file_dir):
    types = dict()
    for root, dirs, files in os.walk(file_dir):
        for file in files:
            splitext = os.path.splitext(file)  # 获取文件名.文件扩展名(元组)
            list = [splitext[0], splitext[1]]
            if list[1] == '':   # 获取文件名.文件扩展名(元组)
                list1 = ['space']
                del splitext
                splitext = tuple(list1+list)
            if len(types):
                flag = 0
                if splitext[1][0] != '.':
                    if types.__contains__('others'):
                        types['others'] += 1
                    else:
                        types['others'] = 1
                        types['others'] += 1
                else:
                    for type in types:
                        if splitext[1] == type:
                            flag = 1
                            types[splitext[1]] += 1
                    if flag == 0:
                        types[splitext[1]] = 1
            else:
                if splitext[1][0] == '.':
                    types[splitext[1]] = 1
                elif splitext[1][0] != '.':  # 只有文件名，没有扩展名，归为others
                    types['others'] = 1
            del list

    return types


# clone-service申请工作区
def clone_repo(local_addr):
    repo_path = REPO_PATH + '/' + local_addr
    clone_path = REPO_PATH + '/clone'
    split = local_addr.split('/')
    user = split[1]
    if not os.path.exists(clone_path + '/' + user):
        os.makedirs(clone_path + '/' + user)
    os.chdir(clone_path)
    os.chdir(user)
    repo_name = local_addr.split('/')[-1]
    if not os.path.exists(clone_path + '/' + user + '/' + repo_name):
        ret = os.system('cp -r %s %s' % (repo_path, repo_name))
        # 备份文件名：repo_name: <repo>-<branch>
        if ret != 0:
            raise Exception('%s : 生成工作区失败！' % repo_name)
