# #!/usr/bin/env python
# # -*- coding:utf-8 -*-
# # !author:bruce chou
# import json
# import os
# import sys
# import time
# import subprocess
# import requests
#
# sys.path.append('/home/fdse/pythonApp/86bruce/restfulAPI/')
#
#
# from config import config
#
# DB = config.ISSUE_TRACKER_MYSQL_DB
#
#
# # 打印时间以及对应的信息
# def log(string):
#     t = time.strftime(r"%Y-%m-%d-%H:%M:%S", time.localtime())
#     print("[%s]%s" % (t, string))
#
#
# # 获取当前git项目的repo所处的commit是哪个
# def log_current_commit(message_get):
#     os.chdir(json.loads(message_get.content).get('data').get('content'))
#     log("当前commit的id为：")
#     batcmd = "git rev-parse HEAD"
#     result = subprocess.check_output(batcmd, shell=True, encoding='utf-8')
#     print(result)
#
#
# # 当前测试项目是 https://github.com/upsnailNostalgia/testGit
#
# # 日志位置
# log_path = '/home/fdse/pythonApp/86bruce/restfulAPI/test/testCodeService.log'
# # 申请资源的url
# url_get = 'http://10.141.221.85:8102/code-service-multi?repo_id=27162b04-5840-11ea-ac71-8f9c5164353e&commit_id='
# # 释放资源的url
# url_free = 'http://10.141.221.85:8102/code-service-multi/free?repo_id=27162b04-5840-11ea-ac71-8f9c5164353e' \
#            '&path='
#
#
# # 测试单次申请资源然后释放情况
# res_get = requests.get(url=url_get)
# log(res_get.content.decode())
# time.sleep(10)
# log_current_commit(res_get)
# res_free = requests.get(url=url_free+json.loads(res_get.content).get('data').get('content'))
# log(res_free.content.decode())
#
#
# # 测试二十次以内（包括二十次）申请资源然后释放情况
# res_get = []
# for i in range(20):
#     res_get.append(requests.get(url=url_get+'6ddd94828317ba80edf39e914212a82979409528'))
#     time.sleep(1)
#
# for i in range(20):
#     log(res_get[i].content.decode())
#     log_current_commit(res_get[i])
#     print()
#
# res_free = []
# for i in range(20):
#     res_free.append(requests.get(url=url_free+json.loads(res_get[i].content).get('data').get('content')))
#     time.sleep(1)
#
# for i in range(20):
#     log(res_free[i].content.decode())
#
#
# # 测试二十次以上申请资源然后释放情况
# res_get = []
# for i in range(25):
#     res_get.append(requests.get(url=url_get+'6ddd94828317ba80edf39e914212a82979409528'))
#     # time.sleep(1)
#
# for i in range(25):
#     log(res_get[i].content.decode())
#     print()
#
# res_free = []
# for i in range(25):
#     res_free.append(requests.get(url=url_free+json.loads(res_get[i].content).get('data').get('content')))
#     # time.sleep(1)
#
# for i in range(25):
#     log(res_free[i].content.decode())
#
#
# # 测试多次申请之后没有资源，返回之后依然释放的情况
# res_get1 = requests.get(url=url_get+'6ddd94828317ba80edf39e914212a82979409528')
# res_get2 = requests.get(url=url_get+'3703536612119bf2cefc421637dace0bbe1eb9ee')
# res_get3 = requests.get(url=url_get+'81f011aadafbd1ac813b202d53761229820ae819')
# res_get4 = requests.get(url=url_get+'457920b491558dacf0ed482574e03f471cbfec1a')
# res_get5 = requests.get(url=url_get+'50ca844cf0c0a5938fc98c3153d024318101b4e3')
# log(res_get1.content.decode())
# log(res_get2.content.decode())
# log(res_get3.content.decode())
# log(res_get4.content.decode())
# log(res_get5.content.decode())
# log_current_commit(res_get1)
# log_current_commit(res_get2)
# log_current_commit(res_get3)
# res_free1 = requests.get(url=url_free+json.loads(res_get1.content).get('data').get('content'))
# res_free2 = requests.get(url=url_free+json.loads(res_get2.content).get('data').get('content'))
# res_free3 = requests.get(url=url_free+json.loads(res_get3.content).get('data').get('content'))
# res_free4 = requests.get(url=url_free+json.loads(res_get4.content).get('data').get('content'))
# log(res_free1.content.decode())
# log(res_free2.content.decode())
# log(res_free3.content.decode())
# log(res_free4.content.decode())
#
#
# # 测试多次申请资源然后释放然后申请交替的情况
# res_get1 = requests.get(url=url_get+'6ddd94828317ba80edf39e914212a82979409528')
# res_get2 = requests.get(url=url_get+'3703536612119bf2cefc421637dace0bbe1eb9ee')
# res_get3 = requests.get(url=url_get+'81f011aadafbd1ac813b202d53761229820ae819')
# log(res_get1.content.decode())
# log(res_get2.content.decode())
# log(res_get3.content.decode())
# log_current_commit(res_get1)
# log_current_commit(res_get2)
# log_current_commit(res_get3)
#
# res_get4 = requests.get(url=url_get+'457920b491558dacf0ed482574e03f471cbfec1a')
#
# res_free1 = requests.get(url=url_free+json.loads(res_get1.content).get('data').get('content'))
# res_free2 = requests.get(url=url_free+json.loads(res_get2.content).get('data').get('content'))
# time.sleep(1)
# res_get5 = requests.get(url=url_get+'50ca844cf0c0a5938fc98c3153d024318101b4e3')
#
# log(res_get4.content.decode())
# log(res_get5.content.decode())
#
# res_free3 = requests.get(url=url_free+json.loads(res_get3.content).get('data').get('content'))
# res_free4 = requests.get(url=url_free+json.loads(res_get4.content).get('data').get('content'))
# res_free5 = requests.get(url=url_free+json.loads(res_get5.content).get('data').get('content'))
# log(res_free1.content.decode())
# log(res_free2.content.decode())
# log(res_free3.content.decode())
# log(res_free4.content.decode())
# log(res_free5.content.decode())
#
#
# # 测试不存在的repo
# url_get_inexist = 'http://10.141.221.85:8102/code-service-multi?repo_id=6666666666666&commit_id='
# res_get1 = requests.get(url=url_get_inexist+'6ddd94828317ba80edf39e914212a82979409528')
# log(res_get1.content.decode())
# # log_current_commit(res_get1)
# res_free1 = requests.get(url=url_free+json.loads(res_get1.content).get('data').get('content'))
# log(res_free1.content.decode())
#
#
# # 测试存在这个repo，但不存在这个commit的id
# res_get1 = requests.get(url=url_get + '66666666666')
# log(res_get1.content.decode())
# # log_current_commit(res_get1)
# res_free1 = requests.get(url=url_free + json.loads(res_get1.content).get('data').get('content'))
# log(res_free1.content.decode())


# path ='/home/fdse/user/issueTracker/repo/github/FudanSELab/IssueTracker-Master-zhonghui20191012_duplicate_fdse-0'
# print(path.split('_duplicate_fdse-')[-2])
# from datetime import datetime
#
# day = datetime.strptime('2019-10-10', '%Y-%m-%d %H:%M')
# print(day)
from redis import Redis
from config import config
REDIS_HOST = config.REDIS['host']
REDIS_DB = config.REDIS['db']
REDIS_PASSWORD = config.REDIS['password']
r = Redis(host=REDIS_HOST, db=REDIS_DB, password=REDIS_PASSWORD)
if r.exists('0b9b3432-53e5-11ea-8463-15fdaaeb85bb-/home/fdse/user/issueTracker/repo/github/upsnailNostalgia/lrfRepos-master_duplicate_fdse-0') == 1:
    print('aaaa')

print(r.keys('*'))
